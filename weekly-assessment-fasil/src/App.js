import "./App.css";
import ThisIsHome from "./Components/ThisIsHome";

function App() {
  return (
    <div className='App'>
      <ThisIsHome />
    </div>
  );
}

export default App;
