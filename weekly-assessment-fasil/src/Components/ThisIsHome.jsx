import React from 'react'

export default function ThisIsHome() {
    return (
        <div className="home">
            <div className="container">
            <img src="https://s3-alpha-sig.figma.com/img/c393/b424/69fbc1ae0c1f3d70d74daf58eef9535f?Expires=1636934400&Signature=Ki5aEQdQcByKii8dE6Y4FDQC2IzPaK2jWm4byUSsqKw6NLzEVItzkJqFAXSzzyih442-p7uaCGWKuWcQMplA7Xn0JQYyo9EGzARSkfk2YnaeZ3tTUB1UauiaQngKEMP4NN6Zguob9IACECiXU4A3HkcFIV0Cz8Q1zwcjFOFutQHaVYssUiikw8jMPxUjmvpt1OVH~AmGzae7HdEz5~BK2ZGp2ZAwSCcj5HhPgY6aseuxBiert004A~y3KMyPigBeJ2aQ~D0YGJP6VOP~lhRlQ~5M1XofLTEztuLfHF3ECRBQS32M~sPuAuSEvjP6Hv8ii1hjvqYyGhHQye6xT6FxQw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" alt="" />
                <div className="le-text">
                    <h1>GOT MARKETING? ADVANCE YOUR BUSINESS INSIGHT.</h1>
                    <p>Fill out the form and receive our award winning newsletter.</p>
                </div>
            </div>
            <div className="le-sign-in">
                <div className="le-text-in">
                    <div className="le-text-box">
                        <div className="le-text-name">
                            <p className="name">Name</p>
                            <label for="le-name"></label>
                            <input type="text" id="le-name" name="name" />
                            <p className="email">Email</p>
                            <label for="le-email"></label>
                            <input type="text" id="le-email" name="email" />
                        </div>
                    </div>
                    <button>Sign me up</button>
                </div>
            </div>
        </div>
    )
}
